#!/usr/bin/php
<?PHP

if (count($argv) < 3 || !is_numeric($argv[1]) || !is_numeric($argv[2]))
{
	print "usage: ./gen_placeholdit.php width height [text]";
	return ;
}
if (isset($argv[3]))
{
	$text = str_replace(" ", "+", $argv[3]);
	print "http://placehold.it/".$argv[1]."x".$argv[2]."&text=".$text.PHP_EOL;
}
else
	print "http://placehold.it/".$argv[1]."x".$argv[2].PHP_EOL;

?>
